package com.advanommen.plugins;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import net.hanjava.svg.SVG2EMF;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
/**
 * Converts SVG files to EMF vector graphics using Batik and FreeHep.
 * @author ad
 */
@Mojo( name = "convert")
public class Svg2EmfMojo extends AbstractMojo
{
    /**
     * A specific <code>fileSet</code> rule to select files and directories.
     */
    @Parameter(required = true)
    private FileSet fileset;
    
    @Parameter(required = true)
    private String outputDir;
    
    public void execute() throws MojoExecutionException
    {
        System.setProperty("org.apache.batik.warn_destination", "false");
        FileSetManager fileSetManager = new FileSetManager();
        String[] includedFileNames = fileSetManager.getIncludedFiles(fileset);
        final File outpPutDirFile = new File(outputDir);
        for (String filename : includedFileNames) {
            try {
                final String svgUrl = "file:///" + new File(fileset.getDirectory(), filename).getAbsolutePath();
                final String resultName = FilenameUtils.removeExtension(filename)+".emf";
                final File resultFile = new File(outpPutDirFile,resultName);
                resultFile.getParentFile().mkdirs();
                SVG2EMF.convert(svgUrl, resultFile);
            } catch (IOException ex) {
                throw new MojoExecutionException("Cannot create EMF", ex);
            }
        }
    }
}