/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.advanommen.permgen.drools;

import org.kie.api.io.ResourceType;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.kie.internal.runtime.StatelessKnowledgeSession;

public class DroolsRunner {

    private static KnowledgeBase kbase;

    public DroolsRunner() {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newClassPathResource("myrule.drl"), ResourceType.DRL);
        kbase = kbuilder.newKnowledgeBase();
    }

    public void runDroolsStateless() {

        StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
        Account account = new Account(200);
        account.withdraw(150);
        ksession.execute(account);
    }

    public void runDroolsStateFul() {
        StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
        Account account = new Account(200);
        account.withdraw(150);
        ksession.insert(account);
        ksession.fireAllRules();
        ksession.dispose();
    }

    void close() {
       kbase = null;
    }
}
