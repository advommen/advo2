package com.advanommen.permgen.drools;

import javax.servlet.ServletContextEvent;

/**
 * Web application lifecycle listener.
 *
 * @author ad
 */
public class ServletContextListener implements javax.servlet.ServletContextListener {

    private DroolsRunner dr;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        dr = new DroolsRunner();
        // dr.runDroolsStateless();
        dr.runDroolsStateFul();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        dr.close();
        dr = null;
    }
}
