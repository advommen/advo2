package com.advanommen.permgen.welbehaved;

import javax.servlet.ServletContextEvent;

/**
 * Web application lifecycle listener.
 *
 * @author ad
 */
public class ServletContextListener implements javax.servlet.ServletContextListener {

    private ClassGenerator cg;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        cg = new ClassGenerator();
        cg.main(new String[0]);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        cg = null;
    }
}
