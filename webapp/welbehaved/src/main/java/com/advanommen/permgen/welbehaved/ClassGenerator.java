package com.advanommen.permgen.welbehaved;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.jboss.vfs.VirtualFile;

public class ClassGenerator {
    private static final int BUFFER = 1024;
    private List<Class<?>> classList = new ArrayList<Class<?>>();

    public void main(String[] args) {
        // ClassGenerator classGenerator = new ClassGenerator();
        // Load just some class with class loaders until perm gen space fills.
        for (int i=0; i<10000; i++) {
            this.classLoader();
        }
        System.out.println("classlist now referencing this number of classes: " + this.classList.size());
    }

    private void classLoader() {
        ClassLoader classLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String classNameWithPackage)
                    throws ClassNotFoundException {
                if (!classNameWithPackage.contains("DummyClass")) {
                    return super.loadClass(classNameWithPackage);
                }
                String className = classNameWithPackage.substring(classNameWithPackage.indexOf("DummyClass"))
                        + ".class";
                byte[] classData = null;
                InputStream inputStream = null;
                try {
                    // inputStream = getResourceAsStream(className);
                    // http://maven.repository.redhat.com/earlyaccess/all/org/jboss/jboss-vfs/3.1.0.Final-redhat-2/
                    inputStream = ((VirtualFile)this.getClass().getResource("").openConnection().getContent()).getChild(className).openStream();
                    byte[] buffer = new byte[BUFFER];
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    int bytesRead = -1;
                    while ((bytesRead = inputStream.read(buffer, 0, BUFFER)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    classData = outputStream.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                Class<?> c = defineClass(classNameWithPackage, classData, 0,
                        classData.length);
                resolveClass(c);
                System.out
                        .println("Loading another copy of Dummy class: " + c);
                classList.add(c);
                return c;
            }
        };

        try {
            Class.forName("com.advanommen.permgen.welbehaved.DummyClass", true, classLoader);
            // Class.forName("com.advanommen.permgen.welbehaved.DummyClass");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}